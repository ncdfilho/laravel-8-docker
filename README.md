# Docker - Laravel

A pretty simplified Docker Compose workflow that sets up a LEMP (Linux, NGINX, MySQL, PHP) network of containers for local Laravel development.

## Ports

```
Ports used in the project:
| **nginx**  |   80 |
| **mysql**  | 3306 |
| **php**    | 9000 |
| **xdebug** | 9001 |
| **redis**  | 6379 |
```

## Use

To get started, make sure you have [Docker installed](https://docs.docker.com/) on your system and [Docker Compose](https://docs.docker.com/compose/install/), and then clone this repository.

### Clone this project:

```
git clone https://ncdfilho@bitbucket.org/ncdfilho/laravel-8-docker.git
```

### Go Inside the folder laravel-8-docker and Generate your own .env to docker compose with the next command:

```
cp .env.example .env
```

### Build the project whit the next commands:

```
docker-compose up -d --build
```

## Within the `laravel-8-docker\source` folder, you should clone the laravel project.

### Follow the next instructions [Here](https://bitbucket.org/ncdfilho/cms.geopal.com/src/master/README.md).

---

## Remember

The configuration of the database **must be the same on both sides** .

```
# .env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_name
DB_USERNAME=db_user
DB_PASSWORD=db_password
DB_ROOT_PASSWORD=secret
```

```
# source/.env
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=db_name
DB_USERNAME=db_user
DB_PASSWORD=db_password
```

The only change is the `DB_HOST` in the `source/.env` where is called to the container of `mysql`:

```
# source/.env
DB_HOST=mysql
```

---

## Special Cases

To Down and remove the volumes we use the next command:

```
docker-compose down -v
```

Update Composer:

```
docker-compose run --rm composer update
```

Run compiler (Webpack.mix.js) or Show the view compiler in node:

```
docker-compose run --rm npm run dev
```
